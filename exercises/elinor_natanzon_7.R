#----------------------- 1 -------------------------#
#--- Run once ---#
# install.packages("ggplot2")

library ("ggplot2")
data(msleep, package="ggplot2")
attach(msleep)

#-------- 1a --------#
brain_body_ratio <- c(brainwt/bodywt)

#----------------------- 2 -------------------------#
#-------- 2a --------#
plot(sleep_total~brain_body_ratio)

#-------- 2b --------#
plot(sleep_total~brain_body_ratio, pch=0)

#----------------------- 3 -------------------------#
#-------- 3a --------#
plot(awake~brain_body_ratio,
     pch=19,
     col="red",
     cex=2,
     xlab="Brain to body wt ration",
     ylab="Active hours")

#-------- 3b --------#
abline(v=0.01, col="blue")

#-------- 3c --------#
text(0.013, 20, "ratio = 1%", col="blue")

#----------------------- 4 -------------------------#
plot(awake~brain_body_ratio,
     log="xy",
     pch=19,
     col="red",
     cex=2,
     xlab="Brain to body wt ration",
     ylab="Active hours",
     main="logged scaled")

#----------------------- 5 -------------------------#
plot(sleep_total~brain_body_ratio, xlim=c(0,0.02))


#---------------------------------------------------#
detach(msleep)