#----------------------- 1 -------------------------#

#-------- 1a --------#
dat_1a <- data.frame(
  name=c("Anastasia", "Dima", "Katherine", "James", "Emily", "Michael", "Matthew",
        "Laura", "Kevin", "Jonas"),
  score=c(12.5,9.0,16.5,12.0,9.0,20.0,14.5,13.5,8.0,19.0),
  attemps=c(1,3,2,3,2,3,1,1,2,1),
  qualify=c('yes','no','yes','no','no','yes','yes','no','no','yes')
)

#-------- 1b --------#
dat_1b <- data.frame(
  dat_1a[2:3, c(1,4)]
)
 
#----------------------- 2 -------------------------#

#-------- 2a --------#
mat_2 <- matrix(nrow=5,ncol=2)

#-------- 2b --------#
mat_2[,1] <- 1:5

#-------- 2c --------#
do_math <- function(x){
  return(x^3 - 1.5)
}

#-------- 2d --------#
#mat_2[,2] <- apply(mat_2, 2, do_math)[,1]
mat_2[,2]<-do_math(mat_2[,1])
#print(mat_2)

#----------------------- 3 -------------------------#

#-------- 3a --------#
dat_3 <- data.frame(mat_2)

#-------- 3b --------#
# => dat_3[,3] = NA
dat_3[,3] <- matrix(nrow=5,ncol=1)

#-------- 3c --------#
names(dat_3) = c("Input", "Ouput", "Logic")

#-------- 3d --------#
#dat_3[,3] <- dat_3[,1] < dat_3[,2]
# => dat_3$logic <- dat_3$input < dat_3$output
dat_3 <- transform(dat_3, "Logic"=dat_3[,1] < dat_3[,2])

#----------------------- 4 -------------------------#

cv <- function(mean, stddev){
  return((mean/stddev)*100)
}

dat_4 <- data.frame(
  Mean = c(10, 2000),
  Standard_Deviation = c(1.5, 150)
)

dat_4 <- transform(dat_4, "CV"= cv(dat_4[,1],dat_4[,2]))
print(dat_4)
