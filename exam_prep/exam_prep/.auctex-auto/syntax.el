(TeX-add-style-hook
 "syntax"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:org4aecbda"
    "sec:orgb767044"
    "sec:orgbb4a516"
    "sec:org017cf01"
    "sec:org1994785"
    "sec:org0e1aa3a"
    "sec:org0de4eec"
    "sec:org8d2fbd1"
    "sec:org49a94fa"
    "sec:org5364fdf"
    "sec:org999b9cb"
    "sec:org55bcd14"
    "sec:org266cb70"
    "sec:org45fefe2"
    "sec:orgde96ed6"
    "sec:org8dcdf67"
    "sec:org4f3a591"
    "sec:orgfdce935"))
 :latex)

