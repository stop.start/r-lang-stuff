dt<- function() {
  dfs <- Filter(function(x) exists(x) && is.data.frame(get(x)), search())
  if(length(dfs)) invisible(sapply(dfs, function(x) detach(x, character.only=T)))
};dt()
rm(list=ls())
cat("\014")  
setwd("~/Documents/School/r_exercises/exam_prep")

for(i in 10:17){
  print(i^3)
}

data_5 <- read.csv("data_ex5.csv") 
data_5 <- subset(data_5, select = -X)

numeric_cols <- unlist(lapply(data_5,is.numeric))
apply(data_5[,numeric_cols],2,mean)
apply(data_5[,numeric_cols],2,sd)

for(name in names(data_5)){
  print(paste(name, nchar(name)))
}

library("ggplot2")
data(msleep, package="ggplot2")

attach(msleep)
print(mean(bodywt[order=="Carnivora"]))
detach(msleep)

print(with(msleep, {mean(bodywt[order=="Carnivora"])}))

while((i<-rnorm(1, mean=0, sd=1))<=1){
  print(i)
}

